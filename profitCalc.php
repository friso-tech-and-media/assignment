<?php

use BundleEngine\Service\BundleProfitRanker;
use BundleEngine\Model\Bundle\Usage;
require './bootstrap.php';

$profit = new BundleProfitRanker();

$profitMatrix = $profit->rankBundleProfit(100 * Usage::DATA_MB);

$fp = fopen('profitMatrix.csv', 'w');

foreach ($profitMatrix as $usage => $rank) {
	outputLine($fp, $rank, $usage);
}

fclose($fp);

/**
 * Output line
 *
 * @param handle $fp
 * @param array $rank
 * @return void
 */
function outputLine(&$fp, $rank, $usage){
	$csvColumns = array();
	array_push($csvColumns, $usage / Usage::DATA_MB . " MB");

	foreach ($rank as $key => $bundle){
		array_push($csvColumns, $bundle['name']);
		array_push($csvColumns, $bundle['profit'] / 100 );
	}

	fputcsv($fp, $csvColumns);
}


?>

