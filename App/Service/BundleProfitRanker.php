<?php
namespace  BundleEngine\Service;

use BundleEngine\Model\Bundle;
use BundleEngine\Model\Bundle\Usage;

final class BundleProfitRanker{

	/**
	 * Mock bundles.
	 *
	 * @var Bundle\Bundle[]
	 */
	private $bundles = [];

	/**
	 * Rank bundles according to nett profit based on data usage
	 *
	 * @param integer $resolution Increments of data to check.
	 * @param integer $max The maximum amount of data usage.
	 * @return void
	 */
	public function rankBundleProfit($resolution = 10000000, $max = 50000000){
		$scale = [];
		$this->mockBundles();

		$ranked = [];

		$currentUsage = 0;

		do {
			usort($this->bundles, array($this, 'sortBundles'));
			$this->addToScale($scale,$currentUsage);

			$this->useData($resolution);
			$currentUsage += $resolution;

		} while ($currentUsage <= $max);
	
		return $scale;
	}	

	/**
	 * Add the current arrangedment of bundles to the scale if the order has been changed.
	 *
	 * @param array $scale
	 * @param integer $currentUsage
	 * @return void
	 */
	private function addToScale(&$scale, $currentUsage){
		$rank = [];

		foreach ($this->bundles as $key=>$bundle){
			$rank[$key]['name'] 	= $bundle->getName();
			$rank[$key]['profit'] 	= $bundle->generateInvoice()->calculateNett() - $bundle->calculateCostOfGoodsSold();
		}
		
		$lastIndex = array_key_last($scale);

		$scale[$currentUsage] = $rank;
	}

	/**
	 * Sort bundles based on profitability
	 *
	 * @param Bundle\Bundle $a
	 * @param Bundle\Bundle $b
	 * @return void
	 */
	private function sortBundles(Bundle\Bundle $a, Bundle\Bundle $b){
		$profitA = $a->generateInvoice()->calculateNett() - $a->calculateCostOfGoodsSold();
		$profitB = $b->generateInvoice()->calculateNett() - $b->calculateCostOfGoodsSold();

		if ($profitA == $profitB)
			return strcmp($a->getName(),$b->getName()); // order alfabetically to ensure same order with equal profits.
		
		return ($profitA > $profitB) ? -1 : 1;
	
	}
	/**
	 * Use up amount of data in all mock bundles.
	 *
	 * @param integer $amount amount of data in KB
	 * @return void
	 */
	private function useData($amount){
		foreach ($this->bundles as $bundle) 
			$bundle->getMobileData()->use($amount);
	}

	/**
	 * Create mock bundles to rank for profitability
	 *
	 * @return void
	 */
	private function mockBundles(){
		$this->bundles[] = new Bundle\BundleA();
		$this->bundles[] = new Bundle\BundleB();
		$this->bundles[] = new Bundle\BundleC();
		$this->bundles[] = new Bundle\BundleD();

		$this->addMockBundleWithBestBundleInsurance( new Bundle\BundleA() );
		$this->addMockBundleWithBestBundleInsurance( new Bundle\BundleB() );
		$this->addMockBundleWithBestBundleInsurance( new Bundle\BundleC() );
		$this->addMockBundleWithBestBundleInsurance( new Bundle\BundleD() );
	}

	/**
	 * Create mock bundles with best bundle insurance on.
	 *
	 * @param Bundle $bundle Fully qualified classname
	 * @return void
	 */
	private function addMockBundleWithBestBundleInsurance(Bundle\Bundle $bundle){
		$bundle->setBestbundleinsurance(true);
		$this->bundles[] = $bundle;
	}	
}

