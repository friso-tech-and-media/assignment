<?php
namespace BundleEngine\Model\Bundle;

/**
 * BundleB contains 1 GB of data for 10 EUR a month
 */
class BundleB extends Bundle {

	protected $dataQuota 		= 1 * 1000 * 1000;
	protected $monthlyCost 	 	= 1000;

}