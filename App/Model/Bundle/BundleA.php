<?php
namespace BundleEngine\Model\Bundle;

/**
 * First bundle, no data but low monthly cost.
 */
class BundleA extends Bundle {

	protected $dataQuota 		= 0;
	protected $monthlyCost 	 	= 100;

}