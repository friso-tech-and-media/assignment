<?php
namespace BundleEngine\Model\Bundle;

/**
 * This class tracks usage and allowed usage
 * 
 */
class Usage{

	
	/**
	 * Quota
	 *
	 * @var integer
	 */
	protected $quota = 0;

	/**
	 * Limit usage amount
	 *
	 * @var integer
	 */
	protected $limitUsage = null;

	/**
	 * Quota used
	 *
	 * @var integer
	 */
	protected $usage = 0;

	/*Data constants for easier calculations */
	const DATA_GB = 1000 * 1000;
	const DATA_MB = 1000;
	const DATA_KB = 1;

	/**
	 * Adjust usage
	 *
	 * @param integer $amount
	 * @return bool Returns if data usage was successfull.
	 */
	public function use($amount){
		if ($this->isLimited() && ($amount + $this->usage) > $this->getLimitUsage())
			return false;

		$this->usage += $amount;
		return true;
	}

	/**
	 * Check if a limit is in place
	 *
	 * @return boolean
	 */
	private function isLimited(){
		return $this->getLimitUsage() !== null;
	}

	/**
	 * Get the amount overdrafted from the allotted amount.
	 *
	 * @return integer
	 */
	public function getOverdraft(){
		return max(0, $this->getUsage() - $this->getQuota());
	}

	public function limitUsageBy($amount){
		$this->setLimitUsage($amount + $this->getUsage() );
	}

	public function removeUsageLimit(){
		$this->limitUsage = null;
	}

	/**
	 * Get limit usage amount
	 *
	 * @return  integer
	 */ 
	public function getLimitUsage()
	{
		return $this->limitUsage;
	}

	/**
	 * Set limit usage amount
	 *
	 * @param integer $limitUsage Limit usage amount
	 */ 
	public function setLimitUsage($limitUsage)
	{
		$this->limitUsage = $limitUsage;
	}

	/**
	 * Get quota used
	 *
	 * @return integer
	 */ 
	public function getUsage()
	{
		return $this->usage;
	}

	/**
	 * Set quota used
	 *
	 * @param  integer  $usage  Quota used
	 */ 
	public function setUsage($usage)
	{
		$this->usage = $usage;
	}

	/**
	 * Get quota
	 *
	 * @return  integer
	 */ 
	public function getQuota()
	{
		return $this->quota;
	}

	/**
	 * Set quota
	 *
	 * @param integer $quota Quota
	 */ 
	public function setQuota($quota)
	{
		$this->quota = $quota;
	}

	/**
	 * Construct a new usage meter
	 *
	 * @param integer $quota
	 * @param integer $used
	 */
	public function __construct($quota = 0, $used = 0)
	{
		$this->quota = $quota;
		$this->used = $used;
	}
}