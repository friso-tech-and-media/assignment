<?php
namespace BundleEngine\Model\Bundle;

use BundleEngine\Model\Bundle\Usage;
use BundleEngine\Model\Product\BillableContract;
use BundleEngine\Model\Product\Invoice;
use BundleEngine\Model\Product\InvoiceLine;
use BundleEngine\Service;
use BundleEngine\Model\Analytics\CostContract;

/**
 * Main bundle product with all the basic functionality required by a bundle product.
 * 
 * @todo implement __clone when a shallow clone doesn't suffice anymore
 */
abstract class Bundle implements BillableContract, CostContract{

	/**
	 * Amount of data allotted in the bundle..
	 *
	 * @var integer
	 */
	protected $dataQuota = 0;

	/**
	 * Money to be charged in euro's when consume data outisde of quota.
	 *
	 * @var integer
	 */
	protected $overdraftCost = 0.01;
	
	/**
	 * Money to be charged for the bundle
	 *
	 * @var integer
	 */
	protected $monthlyCost = 0;

	/**
	 * Dynamically switch bundle.
	 *
	 * @var bool
	 */
	protected $bestBundleinsurance = false;

	/**
	 * Cost for best bundle insurance.
	 *
	 * @var integer
	 */
	protected $bestBundleinsuranceCost = 500;

	/**
	 * Undocumented variable
	 *
	 * @var Usage
	 */
	protected $mobileData;

	/**
	 * The cost of purchasing an MB
	 *
	 * @var integer
	 */
	protected $costPerMB = 17 / 1000; // Guess based on amazon AWS pricing
	/**
	 * Get the value of mobileData
	 * 
	 * @return Usage
	 */ 
	public function getMobileData()
	{
		return $this->mobileData;
	}

	/**
	 * Set mobile data usage.
	 *
	 * @param Usage $mobileData
	 * @return void
	 */
	public function setMobileData($mobileData)
	{
		$this->mobileData = $mobileData;
	}

	/**
	 * Get an invoice for all data used, monthly cost and best bundle insurance.
	 *
	 * @return Invoice
	 */
	public function generateInvoice()
	{	
		$invoice = new Invoice();
		$this->addBundleLine($invoice);

		$this->addBestBundleLines($invoice);
		
		return $invoice;
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function calculateCostOfGoodsSold(){
		return $this->getMobileData()->getUsage() * ($this->costPerMB /  Usage::DATA_MB);
	}

	/**
	 * Add lines related to the bundle.
	 *
	 * @param Invoice $invoice
	 * @return void
	 */
	private function addBundleLine(Invoice $invoice){
		$quotaGB 	= $this->getMobileData()->getQuota() / Usage::DATA_GB; 
		$bundleUsed = min($this->getMobileData()->getUsage() / Usage::DATA_GB, $quotaGB);

		$bundleLine = new InvoiceLine(InvoiceLine::TYPE_MONTHLY_COST, 'Monthly Bundle', "{$bundleUsed} GB / {$quotaGB} GB", $this->monthlyCost);
		$invoice->addLine($bundleLine);

		$overDraftGB 	 = $this->getMobileData()->getOverdraft() / Usage::DATA_MB;
		$overdraftCosts  = $this->getMobileData()->getOverdraft()  * $this->overdraftCost;

		$bundleLine 	 = new InvoiceLine(InvoiceLine::TYPE_OVERDRAFT_COST, 'Out of bundle', "{$overDraftGB} MB", $overdraftCosts);
		$invoice->addLine($bundleLine);
	}

	/**
	 * Process best bundle insurance
	 *
	 * @param Invoice $invoice
	 * @return void
	 */
	private function addBestBundleLines(Invoice $invoice){
		if (!$this->bestBundleinsurance)
			return;

		$bundleLine = new InvoiceLine(InvoiceLine::TYPE_BEST_BUNDLE_COST,'Best bundle insurance', " - ", $this->bestBundleinsuranceCost);
		$invoice->addLine($bundleLine);	
		
		$bestBundleService  = new Service\BestBundle();
		$bestBundle 		= $bestBundleService->selectBestBundle($this);

		if ($bestBundle === true)
			return;

		$rebate = ($invoice->calculateNett() - $this->bestBundleinsuranceCost) - $bestBundle->generateInvoice()->calculateNett();
	
		$bestBundleName = $bestBundle->getName();
		$bestBundleLine = new InvoiceLine(InvoiceLine::TYPE_BEST_BUNDLE_DISCOUNT, "Best bundle rebate", "{$bestBundleName}", -1 * $rebate);
		$invoice->addLine($bestBundleLine);

	}

	/**
	 * Set the best bundle insurance on or off.
	 *
	 * @param bool $on
	 * @return void
	 */
	public function setBestbundleinsurance($on){
		$this->bestBundleinsurance = $on;
	}

	/**
	 * Check if best bundle insurance is active
	 *
	 * @return bool
	 */
	public function getBestBundleInsurance(){
		return $this->bestBundleinsurance;
	}
	
	/**
	 * Get the bundle external name.
	 *
	 * @return string
	 */
	public function getName(){
		$className 	= get_class($this);
		$namespace = explode('\\', $className);
		$name = array_pop($namespace);
		$name = $this->getBestBundleInsurance() ? $name . ' (bb)' : $name;
		return $name;
	}

	/**
	 * Create a new bundle instance
	 */
	public function __construct()
	{
		$this->mobileData = new Usage();
		$this->mobileData->setQuota($this->dataQuota);
	}
}