<?php
namespace  BundleEngine\Repository;

/**
 * The customer repository is resposible for bridging the input and output of the
 * data access layer and the local model. It has not been implemented in this example
 * 
 */
interface Customer{
	public function get($id);
	public function find($id);
	public function list();
	public function listWhere($column, $value);

	public function save(Customer $customer);
	public function delete(Customer $customer);
}